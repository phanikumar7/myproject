package zoomcarPackage;

import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class ZoomcarAnnotation extends SeleniumBase{
 
  @BeforeMethod
  public void login() {
	  
	  startApp("chrome","https://www.zoomcar.com/chennai/");
	  
  }

  @AfterMethod
  public void logout() {
	  close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
