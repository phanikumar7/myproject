package zoomcarPackage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class Zoom extends SeleniumBase {

	@BeforeClass
	public void setdata() {
		testcaseName = "Zoomcar";
		testcaseDes = "Zoomcar Website Automation";
		author = "Phani Kumar";
		category = "smoke";
	}

	@Test
	public void zoomcars() throws InterruptedException {
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement eleSearch = locateElement("link", "Start your wonderful journey");
		click(eleSearch);
		click(locateElement("xpath", "(//div[@class='component-popular-locations']/div)[2]"));
		WebElement clickNext = locateElement("xpath", "//button[text()='Next']");
		click(clickNext);
		// Get current date
		Date date = new Date();

		DateFormat sdf = new SimpleDateFormat("dd");
		// Get today date
		String today = sdf.format(date);
		// Convert to integer
		int tomorrow = Integer.parseInt(today) + 1;
		System.out.println("Tommorow Date is :" + tomorrow);

		WebElement eletmrwDate = locateElement("xpath", "//div[@class='days']/div[2]");
		click(eletmrwDate);
		// WebElement selectDate = locateElement("xpath", "//div[contains(text(),"+tomorrow+")]");
		// click(selectDate);
		click(locateElement("xpath", "//button[text()='Next']"));
		Thread.sleep(2000);
		WebElement elestartDate = locateElement("xpath", "//div[@class='day picked low-price']");
		String startDate = elestartDate.getText();
		System.out.println("Start Date is : " + startDate);
		click(locateElement("xpath", "//button[text()='Done']"));
		/*
		  if(startDate.contains(Integer.toString(tomorrow))) {
		  click(locateElement("xpath", "//button[text()='Done']")); 
		  }
		  else {
		  System.err.println("Start Date doesn't matches the selected Date"); 
		  }
		 */

		List<WebElement> carLists = locateElements("class", "car-listing");
		
		System.out.println("No of Cars dislayed" + carLists.size());
		

		List<Integer> maxList = new ArrayList<Integer>();

		// List<WebElement> carAmount =
		// locateElements("xpath","//div[@class='car-amount']/following::div[@class='price']");
		
		// OR
		
		List<WebElement> price = locateElements("class", "price");

		for (WebElement eachPrice : price) {

			System.out.println(Integer.parseInt(eachPrice.getText().replaceAll("\\D", "")));

			maxList.add(Integer.parseInt(eachPrice.getText().replaceAll("\\D", "")));

		}
		Integer max = Collections.max(maxList);

		Integer mPrice = maxList.get(0);

		System.out.println("First Car Price is : " + mPrice);
		System.out.println("the price of maximum car is " + max);

		WebElement mbrand = locateElement("xpath", "//*[contains(text(),'" + max + "')]");

		String brandName = mbrand.getText();

		System.out.println("the brandName of the " + max + " car is" + brandName);

		WebElement bookButton = locateElement("xpath", "//div[contains(text(),'453')]/following::button");

		click(bookButton);

	}
}
