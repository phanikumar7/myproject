package facebook;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Facebook {
	
	
	public static void main(String args[]) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeOptions op = new ChromeOptions();
		op.addArguments("--Disable-Notifications");
		ChromeDriver driver = new ChromeDriver(op);
		driver.get("https://www.facebook.com"); 
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement username = driver.findElementByXPath("//input[@name='email']");
		username.sendKeys("phanipasupuleti007@gmail.com");
		driver.findElementByXPath("//input[@name='pass']").sendKeys("phani143");;
		driver.findElementByXPath("//label[@id='loginbutton']/input").click();
		driver.findElementByXPath("(//div[@class='_5861 navigationFocus textInput _5eaz']/input)[2]").clear();
		driver.findElementByXPath("(//div[@class='_5861 navigationFocus textInput _5eaz']/input)[2]").sendKeys("TestLeaf");
		driver.findElementByXPath("(//button[@type='submit'])[1]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//li[@class='_5vwz _45hc']//a)[7]").click();
		Thread.sleep(2000);
		String st = driver.findElementByXPath("//a[text()='TestLeaf']").getText();
		System.out.println(st);
		if (st.equalsIgnoreCase("Testleaf")) {
			System.out.println("The Entered name appears correctly");
		}
		else
			System.out.println("The Entered name appears incorrectly");
		String text = driver.findElementByXPath("(//div[@class='_ohf rfloat'])[2]//button").getText();
		if(text.contentEquals("Like"))
		{
			driver.findElementByXPath("(//button[@data-testid='search_like_button_test_id'])[1]").click();
			System.out.println("Button clicked");
		}
		else if(text.contains("Liked"))
			System.out.println("Liked Previously");
			
		driver.findElementByXPath("//a[text()='TestLeaf']").click();
		String exp ="TestLeaf - Home";
		wait.until(ExpectedConditions.titleContains("TestLeaf"));
		String title= driver.getTitle();
		if (title.contains(exp)) {
			System.out.println("Verified the title ,it appears as " + title);
		}
		else
			System.out.println("Verified the title"+title+"its not matching with"+ exp);
		Thread.sleep(3000);	
		
			wait.until(ExpectedConditions.visibilityOf(driver.findElementByXPath("//div[contains(text(),'people like this')]")));

			WebElement noLikes = driver.findElementByXPath("//div[contains(text(),'people like this')]");
			
		String like = noLikes.getText().replaceAll("\\D","");
		System.out.println(like);
		
      driver.close();
	}

}

