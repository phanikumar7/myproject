package coding;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.File;

public class FileCreation {
	public static void main(String[] args) throws IOException {

		// Creating a new file
		String fileSeparator = System.getProperty("user.dir");
		System.out.println(fileSeparator);
		File file = new File("D://Test Leaf/Workspce for java/Yalla/myfile1.txt");
		boolean newFile = file.createNewFile();
		if (newFile) {
			System.out.println("File has been created successfully");
		} else {
			System.out.println("File already present at the specified location");
		}

	}
}
