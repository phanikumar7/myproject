package coding;

public class PrimeNumber extends BaseTestNg {
	static boolean isPrime(int n) {
		for(int i=2;i<n;i++) {
			if(n%i==0)
				return false;
		}
		return true;
	}
	//priority=1
	public static void main(String[] args) {
		boolean prime = isPrime(17);
		System.out.println(prime);
		findingPrime();
	}
	//(priority=2)
	public static void findingPrime() {
		int num = 17, count;

		for (int i = 1; i <= num; i++) {
			count = 0;
			for (int j = 2; j <= i / 2; j++) {
				if (i % j == 0) {
					count++;
					break;
				}
			}
			if (count == 0) {
				System.out.println(i);
			}

		}
	}

}


/*
package week1.day2;

import java.util.Scanner;

public class PrimeNum {

	public static void main(String[] args) {
		
		// To check the range of prime numbers
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Range Number");
		int range = sc.nextInt();
		if((range == 0)||(range == 1)) {
			System.out.println(range+" is not prime number");
		}
		for (int i=2;i<=range;i++) {
			int count = 0;
			for(int j=2;j<i;j++) {
				if(i%j == 0) {
					count = count + 1;
				}
			}
			if(count == 0) {
								
				System.out.print(i+" , ");
			}
		}
		sc.close(); 
		
		// To check single number
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number");
		int num = sc.nextInt();
		int temp = 0;
		if((num == 0)||(num == 1)) {
			System.out.println(num+" is not prime number");
		}
		for(int i=2;i<num;i++) {
			if(num%i == 0) {
				temp = temp + 1;
			}
		}
		if(temp == 0) {
			System.out.println(num+" is Prime Number");
		}
		else {
			System.out.println(num+"Is not Prime Number");
		}
				
		sc.close();
	}

}
*/