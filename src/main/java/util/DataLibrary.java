package util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataLibrary {

	public static Object[][] getExcelData(String excelName) throws IOException { 

		// Load Excel to open
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+excelName+".xlsx");
		// To goto excel sheet
		XSSFSheet sheet = wbook.getSheetAt(0);
		// To get the rowcount
		int lastRowNum = sheet.getLastRowNum();
		System.out.println("Row Count is : " + lastRowNum);
		// To get the coloumn count
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Coloumn Count is : " + colCount);
		// To Iterate in excel cells and get data
		Object[][] data = new Object[lastRowNum][colCount];
		for (int i = 1; i <= lastRowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colCount; j++) {
				XSSFCell cell = row.getCell(j);
				
				String cellValue = cell.getStringCellValue();
				System.out.println(cellValue);
				data[i-1][j] = cellValue;
			}
			
		}
		return data;
	}

}
