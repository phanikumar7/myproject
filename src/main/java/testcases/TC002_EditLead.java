package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TC002_EditLead extends Annotations{
		
	
@BeforeTest(groups="common")

	public void setData() {
		
		testcaseName = "TC002_EditLead";
		testcaseDes = "To Edit the existing lead in leaftaps";
		author = "Phani";
		category = "Sanity";
		excelName = "EditleadData";
	}

	//@Test(dependsOnMethods= {"testcases.TC001_Createlead.CLead"})
	//@Test(dependsOnMethods= {"testcases.TC001_Createlead.CLead"},alwaysRun=true)
	//@Test(timeOut=10000)
	//@Test(groups="sanity")
	//@Test(groups="sanity",dependsOnGroups="smoke")
	
	@Test(dataProvider="fetchdata")
	public void Elead(String fname , String ownership) throws InterruptedException {
		
		
		
		WebElement eleCRM = locateElement("link", "CRM/SFA");
		click(eleCRM);
		locateElement("link", "Leads").click();
		locateElement("link", "Find Leads").click();
		locateElement("xpath", "(//input[@name='firstName'])[3]").sendKeys(fname);
		locateElement("xpath","(//td[@class='x-panel-btn-td'])[6]//button").click();
		Thread.sleep(5000);
		locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-firstName'])[1]").click();
		locateElement("link","Edit").click();
		selectDropDownUsingText(locateElement("id", "updateLeadForm_ownershipEnumId"), ownership);
		locateElement("xpath", "(//input[@class='smallSubmit'])[1]").click();
		String text = locateElement("id", "viewLead_ownershipEnumId_sp").getText();
		System.out.println(text);
		
	}
	


}
