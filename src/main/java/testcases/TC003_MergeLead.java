package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

public class TC003_MergeLead extends Annotations{
	
	@BeforeTest(groups="common")
	
	public void setData() {
		
		testcaseName = "TC003_MergeLead";
		testcaseDes = "To Merge from one lead to anoher lead in leaftaps";
		author = "Phani";
		category = "Regression";
		excelName = "MergeData";
	}
	//@Ignore
	// OR
	//@Test(enabled=false)
	@Test(groups="regression" , dataProvider="fetchdata")
	public void mlead(String fname , CharSequence[] Lid) throws InterruptedException {
		
	
		WebElement eleCRM = locateElement("link", "CRM/SFA");
		click(eleCRM);
		
		locateElement("link", "Leads").click();
		locateElement("link", "Merge Leads").click(); 
		WebElement eleFromLead = locateElement("xpath", "((//td[@class='titleCell'])[1]/following::td/a)[1]");
		click(eleFromLead);
		switchToWindow("Find Leads");
		locateElement("xpath", "(//div[@class='x-form-element']/input)[2]").sendKeys(fname);
		locateElement("xpath", "//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		locateElement("xpath","(//a[@class='linktext'])[3]").click();
		switchToWindow("Merge Leads | opentaps CRM");
		String fromlead = locateElement("xpath", "//input[@name='ComboBox_partyIdFrom']").getAttribute("value");
		System.out.println("From Lead value : "+fromlead);
		
		locateElement("xpath", "((//td[@class='titleCell'])[1]/following::td/a)[2]").click();
		switchToWindow("Find Leads");
		locateElement("xpath", "(//div[@class='x-form-element']/input)[1]").sendKeys(Lid);
		locateElement("xpath", "//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		locateElement("xpath", "(//a[@class='linktext'])[3]").click();
		switchToWindow("Merge Leads | opentaps CRM");
		locateElement("link", "Merge").click();
		//switchToAlert();
		acceptAlert();
		locateElement("link", "Find Leads").click();
		locateElement("xpath", "//input[@name='id']").sendKeys(fromlead);
		locateElement("xpath", "//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		String errtxt = locateElement("xpath","//div[@class='x-paging-info']").getText();
		System.out.println("Error Message : "+errtxt);
		
		if(errtxt.contains("No records to display")) {
			System.out.println("Error text displayed");
			
		}
		
		//driver.close();
		}

	}

