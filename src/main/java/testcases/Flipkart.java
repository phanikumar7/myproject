package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Flipkart {
	
	@Test
	public void bookMobile() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE);
		action.moveToElement(driver.findElement(By.xpath("//span[text()='Electronics']"))).perform();
		driver.findElement(By.linkText("Mi")).click();
		List<WebElement> allMobiles = driver.findElements(By.xpath("//*[@class='_3wU53n']"));
		int total = allMobiles.size();
		
		System.out.println("Total No Of MI Mobiles displayed : "+total);
		
		for (WebElement Brandnames : allMobiles) {
			
			String brand = Brandnames.getText();
			System.out.println("Brand Names : "+brand);
			
		}
		
		List<Integer> li = new ArrayList<Integer>();
		
		List<WebElement> allPrizes = driver.findElements(By.xpath("//*[@class='_1vC4OE _2rQ-NK']"));
		for (WebElement prize : allPrizes) {
			String p = prize.getText().replaceAll("\\D", "");
			System.out.println("Prizes : "+p);
			li.add(Integer.parseInt(p));
		}
		
			/*Object[] array = li.toArray();
			
			//int a[]=new int[allPrizes.size()];
			int max=0;
			System.out.println("The Given Array Element is:");
			for(int i = 0; i < array.length;i++)
			{
				System.out.println(array[i]);
			}
			for(int i = 1; i < array.length;i++)
			{
				
				if(array[i] > max)
				{
					max = a[i];
				}
			}*/
			
			
			
			//System.out.println("From The Array Element Largest Number is:" + max);
			
			
		
		
		
	}

}
