package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class TC004_DuplicateLead extends Annotations{
	
	@BeforeTest(groups="common")
	
	public void setData() {
		
		testcaseName = "TC004_DuplicateLead";
		testcaseDes = "To Duplicate the existing lead in leaftaps";
		author = "Phani";
		category = "Sanity";
	}

	@Test
	public void Dlead() throws InterruptedException {
		
		
		
		WebElement eleCRM = locateElement("link", "CRM/SFA");
		click(eleCRM);
		locateElement("link", "Leads").click();
		locateElement("link", "Find Leads").click();
		locateElement("xpath","(//div[@class='x-tab-strip-wrap']//span/span)[3]").click();
		locateElement("xpath","(//div[@class='x-form-item x-tab-item'])[5]//div/input").sendKeys("dhana@gmail.com");
		locateElement("xpath","(//td[@class='x-panel-btn-td'])[6]//button").click();
		Thread.sleep(2000);
		String text = locateElement("xpath","(//table[@class='x-grid3-row-table']//td[3]//a)[1]").getText();
		System.out.println(text);
		locateElement("xpath","(//table[@class='x-grid3-row-table']//td[3]//a)[1]").click();
		locateElement("link","Duplicate Lead").click();
		String dup = driver.getTitle();
		if(dup.contains("Duplicate Lead | opentaps CRM")) {
			System.out.println("Duplicate Lead Title Displayed");
		}else {
			System.out.println("Duplicate Lead Title is not Displayed");
		}
		locateElement("xpath","//input[@name='submitButton']").click();
		String text2 = locateElement("id","viewLead_firstName_sp").getText();
		if(text.equalsIgnoreCase(text2)) {
			System.out.println("duplicated lead name is same as captured name");
		}else {
			System.out.println("duplicated lead name is not same as captured name");
		}
		
	}

}
