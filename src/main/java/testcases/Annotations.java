package testcases;

import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

import util.DataLibrary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase {

	@DataProvider(name = "fetchdata")
	public Object[][] fetchData() throws IOException{
		
		/*DataLibrary rd = new DataLibrary();
		return rd.getExcelData(excelName);*/
		
		return DataLibrary.getExcelData(excelName);
		
	/*	Object[][] data = new Object[2][3];
		data[0][0] = "TestLeaf";
		data[0][1] = "phani";
		data[0][2] = "kumar";
		
		data[1][0] = "Test";
		data[1][1] = "kalai";
		data[1][2] = "S";*/
		
		
	}
	@Parameters({"url","username","password"})
	@BeforeMethod(groups="common")
	
	public void login(String url , String username , String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);

	}

	@AfterMethod(groups="common")
	public void logout() {
		close();
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
