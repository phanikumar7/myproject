package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class TC001_Createlead extends Annotations {
	
	@BeforeTest(groups="common")
	
	public void setData() {
		
		testcaseName = "TC001_Createlead";
		testcaseDes = "Create a new Lead in leaftaps";
		author = "Phani";
		category = "Smoke";
		excelName = "createleadData";
	}
	
	
	//@Test(invocationCount=2)
	//@Test(invocationCount=2,invocationTimeOut=10000)
	//@Test(priority=2)
	@Test(groups="smoke" , dataProvider="fetchdata")
	public void CLead(String cname , String fname , String lname) {
		
		
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		verifyTitle("Leaftaps - TestLeaf Automation Platform");
		WebElement eleCRM = locateElement("link", "CRM/SFA");
		click(eleCRM);
		
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleCmpyName = locateElement("id", "createLeadForm_companyName");
		eleCmpyName.sendKeys(cname);
		
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		eleFirstName.sendKeys(fname);
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		eleLastName.sendKeys(lname);
		
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Direct Mail");
		
		WebElement eleCreateLeadbutton = locateElement("name", "submitButton");
		click(eleCreateLeadbutton);
	}
	
	/*//@Test(priority=1)
	//@Test
	public void CLead1() {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		verifyTitle("Leaftaps - TestLeaf Automation Platform");
		WebElement eleCRM = locateElement("link", "CRM/SFA");
		click(eleCRM);
		
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleCmpyName = locateElement("id", "createLeadForm_companyName");
		eleCmpyName.sendKeys("HCL");
		
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		eleFirstName.sendKeys("phani");
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		eleLastName.sendKeys("kumar");
		
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Direct Mail");
		
		WebElement eleCreateLeadbutton = locateElement("name", "submitButton");
		click(eleCreateLeadbutton);
	}*/
	
}
